<!DOCTYPE html>
<html>

<body>
    <?php

    include 'validators.php';
    include 'db.php';


    $database = new DB();
    $database->listSongs();



    if (isset($_GET["name"]) || isset($_GET["year"])) {
        echo validateName($_GET["name"]) ? '' : "Please Enter A valid name";
        echo validateYear($_GET["year"]) ? '' : "Please Enter A valid year";
    }
    try {
        if(isset($_GET["name"]) && isset($_GET["year"])) {
        if (validateName($_GET["name"]) && validateYear($_GET["year"])) {
            $database->addSong($_GET["name"], $_GET["year"]);
            header("Location: /bootfi-php-project");
            exit();
        }}
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }


    ?>

    <form action='/bootfi-php-project/' method='GET'>
        <label for="name">Enter a new Song Name</label>
        <input type=text name='name' id='name' />
        </br>
        <label for="year">Enter Release Year</label>
        <input type=number name='year' id='year' />
        <input type='submit' />
    </form>

</body>

</html>