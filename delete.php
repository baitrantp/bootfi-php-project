<?php
include 'db.php';


try {
    $database = new DB();
    $database->deleteSong($_GET["id"]);
    header("Location: /bootfi-php-project");
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
