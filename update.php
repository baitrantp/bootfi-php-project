<?php
include 'db.php';

try {
    $database = new DB();
    $song = $database->getSong($_GET["id"]);

    if (isset($_GET["id"]) && isset($_GET["name"]) && isset($_GET["year"])) {
        $database->editSong($_GET["id"], $_GET["name"], $_GET["year"]);
        header("Location: /bootfi-php-project");
        exit();
    }
    echo '<form action"/bootfi-php-project/update.php= method="GET">';
    echo '<input type=text name="name" value="' . $song[1] . '" >';
    echo '<input type=number name="year" value="' . $song[2] . '" >';
    echo '<input type=hidden name="id" value="' . $song[0] . '" >';

    echo '<input type="submit"/></form>';
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
