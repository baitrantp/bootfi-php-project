<?php
/**
 * @param Number year song release year
 * @return Boolean 
 */
function validateYear($year){ 
    if(!$year){
        return False;
    };
    return ($year>1900 && $year<2020);
  }

  /**
 * @param String song name
 * @return Boolean 
 */
function validateName($name){ 

    $REGEX='/^[A-Za-z]+((\s)?([A-Za-z])+)*$/';
    if(!$name){
        return False;
    };
    return preg_match($REGEX, $name);

  }
  ?>