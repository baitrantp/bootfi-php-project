<?php
include 'constants.php';
/**
 * Database Utility Class
 * @author ali al-baiti <baitrantp@gmail.com>
 */
class DB
{

    private $connection;

    function __construct()
    {
        $this->connect();
    }
    /**
     * connect to database
     */
    public function connect()
    {
        // Create connection
        $this->connection = new mysqli(DB_SERVER, DB_USER_NAME, DB_PASSWORD, DB_NAME);
        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        } else {
            echo "Connected successfully";
        }
    }
    /** 
     * get songs from db
     * @return Array  $songs a list of songs 
     */

    public function listSongs()
    {
        $sql = "SELECT id,name, year FROM Songs";
        $result = $this->connection->query($sql);

        if ($result->num_rows > 0) {
            echo "<table><tr><th>ID</th><th>Name</th><th>Release Year</th></tr>";
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                echo "<tr><td>" . $row["id"] . "</td><td>" . $row["name"] . "</td><td> " . $row["year"] . "</td><td><a href='/bootfi-php-project/update.php/?id=" . $row["id"] . "'>Edit</a></td><td><a href='/bootfi-php-project/delete.php/?id=" . $row["id"] . "'>Delete</a></tr>";
            }
            echo "</table>";
        } else {
            echo "0 results";
        }
    }


    /** 
     * delete song
     * @param Number $id song id
     */

    public function deleteSong($id)
    {
        // sql to delete a record
        $sql = "DELETE FROM Songs WHERE id='$id'";

        if ($this->connection->query($sql) === TRUE) {
            echo "Record deleted successfully";
        } else {
            echo "Error deleting record: " . $this->connection->error;
        }
    }

    /** 
     * edit song
     * @param Number $id song id
     * @param string $name song name
     * @param string $year song release year
     */

    public function editSong($id, $name, $year)
    {
        $sql = "UPDATE Songs  SET name = '$name', year = '$year'  WHERE id='$id'";
        if ($this->connection->query($sql) === TRUE) {
            echo "Record updated successfully";
        } else {
            throw new Exception("Error updating record: " . $this->connection->error);
        }
    }


    /** 
     * get songs from db
     * @return Array  $song details 
     */

    public function getSong($id)
    {
        $sql = "SELECT * FROM Songs WHERE id='" . $id . "'";
        $result = $this->connection->query($sql);
        $song = [];
        if ($result->num_rows > 0) {
            echo "<table><tr><th>ID</th><th>Name</th><th>Release Year</th></tr>";
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                echo "<tr><td>" . $row["id"] . "</td><td>" . $row["name"] . "</td><td> " . $row["year"] . "</td><td><a href='/bootfi-php-project/update.php/?id=" . $row["id"] . "'>Edit</a></td></tr>";
                array_push($song, $row["id"], $row["name"], $row["year"]);
            }
            echo "</table>";
        } else {
            echo "not found";
        }

        return $song;
    }
    /** 
     * add a new song
     * @param string $name song name
     * @param string $year song release year
     *  
     */

    public function addSong($name, $year)
    {
        $year = strval($year);
        $sql = "INSERT INTO `songs` (`name`, `year`)VALUES ('" . $name . "','" . $year . "')";
        if ($this->connection->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $this->connection->error;
        }
    }
}
